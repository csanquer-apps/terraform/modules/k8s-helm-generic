variable "name" {
  description = "Helm Release name."
  type        = string
}

variable "description" {
  description = "Helm Release description attribute."
  type        = string
  default     = null
}

variable "chart" {
  description = "Helm chart name to be installed."
  type        = string
}

variable "chart_version" {
  description = "Helm chart version to install. If this is not specified, the latest version is installed."
  type        = string
  default     = null
}

variable "repo_url" {
  description = "Helm Repository URL where to locate the requested chart."
  type        = string
  default     = null
}

variable "repo_key_file" {
  description = "Helm Repository certificate key file."
  type        = string
  default     = null
}

variable "repo_cert_file" {
  description = "Helm Repository certificate file."
  type        = string
  default     = null
}

variable "repo_ca_file" {
  description = "Helm Repository certificate authority file."
  type        = string
  default     = null
}

variable "repo_username" {
  description = "Helm Repository HTTP basic authentication username."
  type        = string
  default     = null
}

variable "repo_password" {
  description = "Helm Repository HTTP basic authentication password."
  type        = string
  sensitive   = true
  default     = null
}

variable "namespace" {
  description = "kubernetes namespace to install the release into."
  type        = string
  default     = "default"
}

variable "create_namespace" {
  description = "create kubernetes namespace."
  type        = bool
  default     = false
}

variable "namespace_labels" {
  description = "kubernetes namespace labels if creating namespace."
  type        = map(string)
  default     = {}
}

variable "namespace_annotations" {
  description = "kubernetes namespace annotations if creating namespace."
  type        = map(string)
  default     = {}
}

variable "verify" {
  description = "Verify the package before installing it. Helm uses a provenance file to verify the integrity of the chart; this must be hosted alongside the chart."
  type        = bool
  default     = false
}

variable "keyring" {
  description = "Location of public keys used for verification. Used only if verify is true. Defaults to /.gnupg/pubring.gpg in the location set by home."
  type        = string
  default     = null
}

variable "lint" {
  description = "Run the helm chart linter during the plan."
  type        = bool
  default     = false
}

variable "max_history" {
  description = "Maximum number of release versions stored per release. if 0 there is no limit"
  type        = number
  default     = 0
}

variable "disable_webhooks" {
  description = "Prevent hooks from running."
  type        = bool
  default     = false
}

variable "reuse_values" {
  description = "When upgrading, reuse the last release's values and merge in any overrides. If 'reset_values' is specified, this is ignored."
  type        = bool
  default     = false
}

variable "reset_values" {
  description = "When upgrading, reset the values to the ones built into the chart."
  type        = bool
  default     = false
}

variable "force_update" {
  description = "Force resource update through delete/recreate if needed."
  type        = bool
  default     = false
}

variable "recreate_pods" {
  description = "Perform pods restart during upgrade/rollback."
  type        = bool
  default     = false
}

variable "cleanup_on_fail" {
  description = "Allow deletion of new resources created in this upgrade when upgrade fails."
  type        = bool
  default     = false
}

variable "atomic" {
  description = "If set, installation process purges chart on fail. The wait flag will be set automatically if atomic is used."
  type        = bool
  default     = false
}

variable "wait" {
  description = "Will wait until all resources are in a ready state before marking the release as successful. It will wait for as long as timeout."
  type        = bool
  default     = true
}

variable "timeout" {
  description = "Time in seconds to wait for any individual kubernetes operation (like Jobs for hooks)."
  type        = number
  default     = 300
}

variable "skip_crds" {
  description = "If set, no CRDs will be installed. By default, CRDs are installed if not already present. "
  type        = bool
  default     = false
}

variable "render_subchart_notes" {
  description = "If set, render subchart notes along with the parent."
  type        = bool
  default     = true
}

variable "disable_openapi_validation" {
  description = "If set, the installation process will not validate rendered templates against the Kubernetes OpenAPI Schema."
  type        = bool
  default     = false
}

variable "dependency_update" {
  description = "Runs helm dependency update before installing the chart."
  type        = bool
  default     = false
}

variable "replace" {
  description = "Re-use the given name, even if that name is already used. This is unsafe in production."
  type        = bool
  default     = false
}

variable "postrender_binary_path" {
  description = "relative or full path to postrender command binary. It configure a command to run after helm renders the manifest which can alter the manifest contents."
  type        = string
  default     = ""
}

variable "default_values" {
  description = "Helm default values."
  default     = {}
}

variable "values" {
  description = "Helm values (merged with default values)."
  default     = {}
}

variable "custom_values" {
  description = "Helm custom values to be merged with the values yaml."
  default = [
    # {
    #   name = "" # YAML key with dot notation
    #   value = ""
    #   type = "auto" # auto or string
    # }
  ]
}

variable "sensitive_values" {
  description = "Helm custom sensitive values to be merged with the values yaml."
  default = [
    # {
    #   name = "" # YAML key with dot notation
    #   value = ""
    #   type = null # auto or string
    # }
  ]
}
