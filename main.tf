resource "kubernetes_namespace" "this" {
  count = var.create_namespace ? 1 : 0
  metadata {
    name        = var.namespace
    labels      = var.namespace_labels
    annotations = var.namespace_annotations
  }
}

resource "helm_release" "this" {
  name             = var.name
  description      = var.description
  chart            = var.chart
  version          = var.chart_version
  create_namespace = false
  namespace        = var.create_namespace ? kubernetes_namespace.this[0].metadata[0].name : var.namespace

  repository           = var.repo_url
  repository_key_file  = var.repo_key_file
  repository_cert_file = var.repo_cert_file
  repository_ca_file   = var.repo_ca_file
  repository_username  = var.repo_username
  repository_password  = var.repo_password

  lint    = var.lint
  verify  = var.verify
  keyring = var.keyring

  max_history = var.max_history

  reuse_values     = var.reuse_values
  reset_values     = var.reset_values
  force_update     = var.force_update
  recreate_pods    = var.recreate_pods
  cleanup_on_fail  = var.cleanup_on_fail
  disable_webhooks = var.disable_webhooks
  skip_crds        = var.skip_crds

  atomic  = var.atomic
  wait    = var.wait
  timeout = var.timeout

  render_subchart_notes      = var.render_subchart_notes
  disable_openapi_validation = var.disable_openapi_validation

  dependency_update = var.dependency_update
  replace           = var.replace

  dynamic "postrender" {
    for_each = var.postrender_binary_path != "" ? [var.postrender_binary_path] : []
    content {
      binary_path = postrender.value
    }
  }

  values = [
    yamlencode(var.default_values),
    yamlencode(var.values),
  ]

  dynamic "set" {
    for_each = var.custom_values
    content {
      name  = set.value.name
      value = lookup(set.value, "value", "")
      type  = lookup(set.value, "type", null)
    }
  }

  dynamic "set_sensitive" {
    for_each = var.custom_values
    content {
      name  = set_sensitive.value.name
      value = lookup(set_sensitive.value, "value", "")
      type  = lookup(set_sensitive.value, "type", null)
    }
  }
}
