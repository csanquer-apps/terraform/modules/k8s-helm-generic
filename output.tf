output "infos" {
  value = {
    name          = helm_release.this.metadata[0].name
    chart         = helm_release.this.metadata[0].chart
    namespace     = helm_release.this.metadata[0].namespace
    chart_version = helm_release.this.metadata[0].version
    app_version   = helm_release.this.metadata[0].app_version
  }
}

output "name" {
  value = helm_release.this.metadata[0].name
}

output "chart" {
  value = helm_release.this.metadata[0].chart
}

output "namespace" {
  value = helm_release.this.metadata[0].namespace
}

output "chart_version" {
  value = helm_release.this.metadata[0].version
}

output "app_version" {
  value = helm_release.this.metadata[0].app_version
}

output "values" {
  sensitive = true
  value     = helm_release.this.metadata[0].values
}

# output "manifest" {
#   sensitive = true
#   value = helm_release.this.metadata.manifest
# }
